===========================================
Guide du développeur openMairie version 4.9
===========================================

.. image:: http://readthedocs.org/projects/omframework/badge/?version=4.9
    :target: http://openmairie.readthedocs.io/projects/omframework/fr/4.9/?badge=4.9
    :alt: Documentation Status


Génération
==========

Pour générer cette documentation, en HTML :

.. code-block:: sh

  make html

