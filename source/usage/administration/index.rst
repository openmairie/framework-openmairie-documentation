.. _administration:

##############
Administration
##############

Cette rubrique est dédiée à l'administration des fonctionnalités disponibles
depuis l'interface : les tableaux de bord, les éditions, le module om_sig,
...

====================
Les tableaux de bord
====================

Exemple de tableau de bord :

.. image:: a_tableau_de_bord_exemple.png


Widget
------

Le widget (WIDGET DASHBOARD) est un bloc d'informations contextualisées accessible depuis le tableau de bord de l'utilisateur. Il peut être de type 'Web' ou de type 'Script' et peut gérer toutes sortes d'informations internes ou externes à l'application (les tâches non soldées pour openCourrier, les appels à la maintenance, l'horoscope, la météo, une vidéo, des photos, ...).

Le listing des widgets
======================

.. image:: a_tableau_de_bord_widget_listing.png

L'ajout de widgets
==================

Depuis le listing on peut accéder à l'interface d'ajout des widgets. Ce formulaire permet de choisir :

* Le **libelle**  du widget
* Son **type** : qui correspondent à sa source si c'est **file** ce sera un script hébergé sur le serveur si c'est **web** ce sera un lien cliquable

.. image:: a_tableau_de_bord_widget_ajout_web.png

* Si son **type** est **web** (comme sur la capture d'écran ci-dessus)
* Le **lien** : qui pointera vers la page désirée
* Le **texte** : qui sera la description du lien

.. image:: a_tableau_de_bord_widget_ajout_file.png

* Si son **type** est **file** (comme sur la capture d'écran ci-dessus)
* Le **script** : qui sera une liste déroulante des scripts disponibles sur le serveur
* Les **arguments** : qui seront éventuellement interprétés par le script sélectionné


Quelques exemples de widgets 'web'
==================================

Les widgets peuvent être utilisés comme des raccourcis vers des fonctions existantes de l'application, par exemple pour proposer des liens vers des cartes (à mettre dans le champ lien).

Les widgets peuvent être utilisés pour charger des éléments via des requêtes "ajax". Le code présenté en exemple permet de charger dans le widget le code html rendu par l'URL fournie dans le code javascript (Attention, il faut changer l'id pour chaque widget de ce type, ici aff3). ::

    <script type='text/javascript'>
        $.ajax({
            type: 'GET',
           url:'../app/tab_wid.php',  
           cache: false,
           data: '&obj=tachenonsolde_service',
            success: function(html){
                $('#aff3').append(html);
            }
        });
    </script>
    <div id='aff3'></div>


Les widgets peuvent aussi être utilisés pour charger des éléments provenant de sources externes.

Acces à une video externe avec un "iframe" ::

    <iframe width='200' height='150'
        src='http://www.youtube.com/embed/gS5B4LlqkfI'
        frameborder='0' allowfullscreen>
    </iframe>


La meteo grace à un javascript du site tameteo.com ::

    <div id='cont_f5089b722555454d1872b91f52beafd4'>
        <h2 id='h_f5089b722555454d1872b91f52beafd4'>
        <a href='http://www.tameteo.com/' title='Météo'>Météo</a></h2>
        <a id='a_f5089b722555454d1872b91f52beafd4'
            href='http://www.tameteo.com/
                        meteo_Arles-Europe-France-Bouches+du+Rhone--1-25772.html'
            target='_blank' title='Météo Arles'
            style='color:#666666;font-family:1;font-size:14px;'></a>
        <script type='text/javascript'
            src='http://www.tameteo.com/wid_loader/f5089b722555454d1872b91f52beafd4'>
        </script>
    </div>


L'horoscope au travers d un iframe qui pointe sr astroo.com ::

    <!--DEBUT CODE ASTROO-->
    <!--debut code perso-->
    <iframe width='232' height='302' marginheight='0' marginwidth='0' frameborder='0'
        align='center' src='http://www.astroo.com/horoscope.htm'
        name='astroo' allowtransparency='true'>
    <!--fin code perso-->
    <a href='http://www.astroo.com/horoscope.php' target='_top'
        title='Cliquez-ici pour afficher l'horoscope quotidien'>
        <font face='Verdana' size='2'><b>afficher l'horoscope du jour</b>
        </font></a>
    </iframe>
    <noscript>
    <a href='http://www.astroo.com/horoscope.php' target='_blank'>horoscope</a>
    </noscript>
    <!--FIN CODE ASTROO-->


Acces à un fil rss avec un module ajax google ::

    <script src='http://www.gmodules.com/ig/ifr?url=
       http://www.ajaxgaier.com/iGoogle/rss-reader%2B.xml
       &up_title=Actualit%C3%A9s%20atReal
       &up_feed=http%3A%2F%2Fwww.atreal.fr%2Fatreal%2Fcommunaute%2Factualites-atreal%2FRSS
       &up_contentnr=9&up_fontsize=9&up_lineheight=70
       &up_titlelink=&up_bullet=1
       &up_reload_feed=0&up_reload_fqcy=0
       &up_hl_background=FFFFFF&synd=open&w=200&h=100
       &title=
       &border=%23ffffff%7C3px%2C1px+solid+%23999999&output=js'>
    </script>


Affichage de photos avec flick 'r (appel javascript)::

    <table><tr>
    <div class='flick_r'>
    <script type='text/javascript'
        src='http://www.flickr.com/badge_code_v2.gne?count=3
            &display=latest&size=s
            &layout=h&source=user
            &user=27995901%40N03'></script>
    </div>
    </tr></table>


Composition
-----------

L'administrateur choisit les widgets présents sur le tableau de bord de chaque profil parmi ceux proposés dans l'application. Il peut placer les widgets où il le souhaite. La composition du tableau de bord se fait depuis l'écran "composer le tableau de bord" du menu administration.

La première étape est la sélection du profil duquel on souhaite configurer le tableau de bord.

.. image:: a_tableau_de_bord_composition_select_profil.png

Puis les actions d'ajout de widget, de suppression de widget, de déplacement de widget (drag and drop) sont disponibles pour configurer le tableau de bord du profil sélectionné.

.. image:: a_tableau_de_bord_composition_exemple.png


============
Les éditions
============

États et lettres types
----------------------


.. warning::

   Cette rubrique est en cours de rédaction.


.. _editions_etat_lettretype_positionnement:

Positionnement des éléments dans l'édition
==========================================

.. image:: m_editions_etat_lettretype_positionnement.png

1. Marge gauche (en millimètre) de l'édition depuis la limite gauche de la page (voir :ref:`editions_etat_lettretype_bloc_edition`) qui concerne les blocs : :ref:`editions_etat_lettretype_bloc_en-tete`, :ref:`editions_etat_lettretype_bloc_corps`, :ref:`editions_etat_lettretype_bloc_pied-de-page`.
2. Marge haute (en millimètre) de l'édition depuis la limite haute de la page (voir :ref:`editions_etat_lettretype_bloc_edition`) qui concerne le bloc : :ref:`editions_etat_lettretype_bloc_corps`.
3. Marge droite (en millimètre) de l'édition depuis la limite droite de la page (voir :ref:`editions_etat_lettretype_bloc_edition`) qui concerne les blocs : :ref:`editions_etat_lettretype_bloc_en-tete`, :ref:`editions_etat_lettretype_bloc_corps`, :ref:`editions_etat_lettretype_bloc_pied-de-page`.
4. Marge basse (en millimètre) de l'édition  depuis la limite basse de la page (voir :ref:`editions_etat_lettretype_bloc_edition`) qui concerne le bloc : :ref:`editions_etat_lettretype_bloc_corps`.
5. Espacement (en millimètre) entre le plafond du bloc 'en-tête' et la limite haute de la page (voir :ref:`editions_etat_lettretype_bloc_en-tete`).
6. Espacement (en millimètre) entre le plafond du bloc 'pied de page' et la limite basse de la page (voir :ref:`editions_etat_lettretype_bloc_pied-de-page`).
7. position du coin haut/gauche du logo par rapport au coin haut/gauche de l'édition (voir :ref:`editions_etat_lettretype_bloc_edition`).
8. position du coin haut/gauche du logo par rapport au coin haut/gauche de l'édition (voir :ref:`editions_etat_lettretype_bloc_edition`).
9. Espacement (en millimètre) entre la paroi gauche du bloc 'titre' et la limite gauche de la page (voir :ref:`editions_etat_lettretype_bloc_titre`).
10. Espacement (en millimètre) entre le plafond du bloc 'titre' et la limite haute de la page  (voir :ref:`editions_etat_lettretype_bloc_titre`).
11. Largeur (en millimètre) du bloc 'titre' depuis la paroi gauche du bloc 'titre' (voir :ref:`editions_etat_lettretype_bloc_titre`).


.. _editions_etat_lettretype_bloc_edition:

Bloc 'édition'
==============

.. image:: a_editions_etat_lettretype_bloc_edition.png

Les informations d'**édition** à saisir sont :

* **id** : identifiant de l'état/lettre type.
* **libellé** : libellé affiché dans l'application lors de la sélection d'une édition.
* **actif** : permet de définir si l'édition est active ou non.

.. note::

    Les champs **id** et **libellé** sont obligatoires, les **id** actif sont uniques.


--------------------------------
Paramètres généraux de l'édition
--------------------------------

Les champs de **paramètres généraux de l'édition** à saisir sont :

* **Orientation** : orientation de l'édition (portrait/paysage).
* **Format** : format de l'édition (A4/A3).
* **Logo** : sélection du logo depuis la table des logos configurés.
* **Logo haut** : position du coin haut/gauche du logo par rapport au coin haut/gauche de l'édition (voir 8 dans :ref:`editions_etat_lettretype_positionnement`).
* **Logo gauche** : position du coin haut/gauche du logo par rapport au coin haut/gauche de l'édition (voir 7 dans :ref:`editions_etat_lettretype_positionnement`).
* **Marge gauche** : Marge gauche (en millimètre) de l'édition depuis la limite gauche de la page (voir 1 dans :ref:`editions_etat_lettretype_positionnement`).
* **Marge haut** : Marge haute (en millimètre) de l'édition depuis la limite haute de la page (voir 2 dans :ref:`editions_etat_lettretype_positionnement`).
* **Marge droite** : Marge droite (en millimètre) de l'édition depuis la limite droite de la page (voir 3 dans :ref:`editions_etat_lettretype_positionnement`).
* **Marge bas** : Marge basse (en millimètre) de l'édition  depuis la limite basse de la page (voir 4 dans :ref:`editions_etat_lettretype_positionnement`).


.. _editions_etat_lettretype_bloc_en-tete:

Bloc 'en-tête'
==============

.. image:: a_editions_etat_lettretype_bloc_en-tete.png

Ce bloc est facultatif et a pour particularité de se répéter sur chaque page.

* **Espacement** : Espacement (en millimètre) entre le plafond du bloc 'en-tête' et la limite haute de la page (voir 5 dans :ref:`editions_etat_lettretype_positionnement`).
* **En-tête** : éditeur riche permettant une mise en page complexe.

Limitation(s) :

- les marges gauche et droite ne sont pas dissociables de celles du corps,
- la hauteur n'est pas fixable, elle dépend du contenu positionné à l'intérieur.


.. _editions_etat_lettretype_bloc_titre:

Bloc 'titre'
============

.. image:: a_editions_etat_lettretype_bloc_titre.png

Ce bloc est obligatoire.

* **Titre** : éditeur riche permettant une mise en page complexe.

--------------------------------
Paramètres du titre de l'édition
--------------------------------

Positionnement :

* **Titre gauche** : Espacement (en millimètre) entre la paroi gauche du bloc 'titre' et la limite gauche de la page (voir 9 dans :ref:`editions_etat_lettretype_positionnement`).
* **Titre haut** : Espacement (en millimètre) entre le plafond du bloc 'titre' et la limite haute de la page (voir 10 dans :ref:`editions_etat_lettretype_positionnement`).
* **Largeur de titre** : Largeur (en millimètre) du bloc 'titre' depuis la paroi gauche du bloc 'titre' (voir 11 dans :ref:`editions_etat_lettretype_positionnement`).
* **Hauteur** : hauteur minimum du titre.

Bordure :

* **bordure** : Affichage ou non d'une bordure.


.. _editions_etat_lettretype_bloc_corps:

Bloc 'corps'
============

.. image:: a_editions_etat_lettretype_bloc_corps.png

Ce bloc est obligatoire.

* **Corps** : éditeur riche permettant une mise en page complexe.

-------------------------
Paramètres des sous-états
-------------------------

* **Police personnalisée** : sélection de la police des sous-états.
* **Couleur texte** : sélection de la couleur du texte des sous-états.


.. _editions_etat_lettretype_bloc_pied-de-page:

Bloc 'pied de page'
===================

.. image:: a_editions_etat_lettretype_bloc_pied-de-page.png

Ce bloc est facultatif et a pour particularité de se répéter sur chaque page.

* **Espacement** : Espacement (en millimètre) entre le plafond du bloc 'pied de page' et la limite basse de la page (voir 6 dans :ref:`editions_etat_lettretype_positionnement`).
* **Pied de page** : éditeur riche permettant une mise en page complexe.

Limitation(s) :

- les marges gauche et droite ne sont pas dissociables de celles du corps,
- la hauteur n'est pas fixable, elle dépend du contenu positionné à l'intérieur.


.. _editions_etat_lettretype_bloc_champs-de-fusion:

Bloc 'champs de fusion'
=======================

.. image:: a_editions_etat_lettretype_bloc_champs-de-fusion.png

* **Requête** : sélection d'un jeu de champs de fusion.
* **Champs de fusion** : Liste des champs de fusion disponibles pour la requête sélectionnée.
* **Variables de remplacement** : Liste des variables de remplacements disponibles.


.. _editions_etat_lettretype_editeur-texte-riche:

Aide à la saisie dans les éditeurs de texte riche
=================================================

.. _editions_etat_lettretype_editeur-texte-riche-configurations:

--------------------------
Configurations disponibles
--------------------------

Trois configurations différentes de l'éditeur de texte riche sont utilisées :

- configuration n°1 : :ref:`editions_etat_lettretype_bloc_corps`,
- configuration n°2 : :ref:`editions_etat_lettretype_bloc_en-tete`, :ref:`editions_etat_lettretype_bloc_titre`, :ref:`editions_etat_lettretype_bloc_pied-de-page`,
- configuration n°3 : blocs de texte avec une mise en forme limitée toujours destinés à être intégré dans une édition via un champs de fusion.


+--------------------------------------------------------------------------------+--------+--------+--------+
| Fonction / Configuration                                                       | N°1    | N°2    | N°3    |
+================================================================================+========+========+========+
| :ref:`editions_etat_lettretype_editeur-texte-riche-tableaux`                   | x      | x      |        |
+--------------------------------------------------------------------------------+--------+--------+--------+
| :ref:`editions_etat_lettretype_editeur-texte-riche-tableaux-insecables`        | x      |        |        |
+--------------------------------------------------------------------------------+--------+--------+--------+
| :ref:`editions_etat_lettretype_editeur-texte-riche-sauts-de-page`              | x      |        |        |
+--------------------------------------------------------------------------------+--------+--------+--------+
| :ref:`editions_etat_lettretype_editeur-texte-riche-code-barres`                | x      | x      |        |
+--------------------------------------------------------------------------------+--------+--------+--------+
| :ref:`editions_etat_lettretype_editeur-texte-riche-majuscule-minuscule`        | x      | x      | x      |
+--------------------------------------------------------------------------------+--------+--------+--------+
| :ref:`editions_etat_lettretype_editeur-texte-riche-sous-etats`                 | x      |        |        |
+--------------------------------------------------------------------------------+--------+--------+--------+
| :ref:`editions_etat_lettretype_editeur-texte-riche-plein-ecran`                | x      | x      |        |
+--------------------------------------------------------------------------------+--------+--------+--------+
| :ref:`editions_etat_lettretype_editeur-texte-riche-code-source`                | x      | x      | x      |
+--------------------------------------------------------------------------------+--------+--------+--------+
| :ref:`editions_etat_lettretype_editeur-texte-riche-correction-orthographique`  | x      | x      | x      |
+--------------------------------------------------------------------------------+--------+--------+--------+


.. _editions_etat_lettretype_editeur-texte-riche-tableaux:

--------------------
Gestion des tableaux
--------------------

Cette aide à la saisie n'est pas nécessairement disponible dans toutes les configurations de l'éditeur de texte riche (voir :ref:`editions_etat_lettretype_editeur-texte-riche-configurations`).

* **Créer un tableau** :

Choisir le nombre de lignes et de colonnes du tableau.

.. note::

    Il faut bien placer le curseur dans une des cellules du tableau que l'on
    souhaite paramétrer.
    Idem pour le paramétrage des lignes et colonnes.

* **Paramétrage général du tableau** :

    - Largeur :

    Ce champ sert à indiquer la largeur du tableau en % (UNIQUEMENT) par rapport
    à la largeur du PDF.

    Par exemple, si le PDF fait une largeur de 30 cm et que la lageur du tableau
    est de 10%, le tableau fera 3 cm de largeur sur le PDF.

    - Hauteur :

    Ce champ sert à indiquer la hauteur du tableau en % (UNIQUEMENT) par rapport
    à la hauteur du PDF.

    Par exemple, si le PDF fait une hauteur de 50 cm et que la hauteur du tableau
    est de 25%, le tableau fera 12.5 cm de hauteur sur le PDF.

    - Espacement inter-cellules :

    Espacement entre les cellules. En pixel.

    - Espace interne cellule :

    Espacement entre les bords de la cellule et son contenu. En pixel.

    - Bordure :

    Epaisseur des bordures du tableau. En pixel.

    - Titre :

    Lorsque cette case est cochée, elle permet de rajouter un titre au tableau.

    - Alignement :

    Permet de choisir le type d'alignement du texte dans le tableau.
    Valeurs possibles : n/a (aucun), Gauche, Centré, Droite.

* **Supprimer un tableau**


* **Paramétrage des cellules** :

    - Largeur :

    Ce champ sert à indiquer la largeur de la colonne en % (UNIQUEMENT) par
    rapport à la largeur du tableau.

    Par exemple, si le tableau fait une largeur de 30 cm et que la largeur de la
    colonne est de 10%, la colonne fera 3 cm de largeur.

    - Hauteur :

    Ce champ sert à indiquer la hauteur de la colonne en % (UNIQUEMENT) par
    rapport à la hauteur du tableau.

    Par exemple, si le tableau fait une hauteur de 50 cm et que la hauteur de la
    colonne est de 25%, la colonne fera 12.5 cm de hauteur.

    - Type de cellule :

    Permet de définir si c'est une cellule "normale" ou une cellule qui va servir
    d'en-tête dans le tableau.
    Valeurs possibles : Cellule, Cellule d'en-tête.

    - Étendue :

    Paramètre sur quoi doivent s'appliquer les paramètres renseignés.
    Valeurs possibles : n/a (aucun), Ligne, Colonne, Groupe de lignes, Groupe de
    colonnes.

    - Alignement :

    Permet de choisir le type d'alignement du texte dans la cellule.
    Valeurs possibles : n/a (aucun), Gauche, Centré, Droite.

* **Fusionner des cellules** :

En sélectionnant les cellules à fusionner et en cliquant sur
Tableau → Cellule → Fusionner les cellules les cellules seront fusionnées.

Si aucune cellule n'est sélectionnée, un menu apparaît :

    - Colonnes :

    Nombre de colonnes qui vont être fusionnées à partir de la cellule dans
    laquelle le curseur est positionné.

    - Lignes :

    Nombre de lignes qui vont être fusionnées à partir de la cellule dans
    laquelle le curseur est positionné.


* **Diviser les cellules** :

Divise la cellule dans laquelle le curseur est positionné si elle avait été
fusionnée avant.


* **Paramétrage des lignes** :

    - Type de ligne :

    Permlet de définir le type de la ligne.
    Valeurs possibles : En-tête, Corps, Pied.

    - Alignement :

    Permet de choisir le type d'alignement du texte dans la ligne.
    Valeurs possibles : n/a (aucun), Gauche, Centré, Droite.

    - Hauteur :

    Ce champ sert à indiquer la hauteur de la ligne en % (UNIQUEMENT) par
    rapport à la hauteur du tableau.

    Par exemple, si le tableau fait une hauteur de 50 cm et que la hauteur de la
    ligne est de 25%, la ligne fera 12.5 cm de hauteur.


* **Insérer une ligne** :

Permet d'insérer une ligne avant ou après la ligne sur laquelle le curseur est
positionné.


* **Effacer une ligne** :

Supprimer la ligne sur laquelle le curseur est positionné.

* **Couper une ligne** :

Coupe la ligne sur laquelle le curseur est positionné.

* **Copier une ligne** :

Copie la ligne sur laquelle le curseur est positionné.


* **Coller une ligne** :

Colle la ligne qui avait été copiée/coupée avant ou après la ligne sur laquelle
le curseur est positionné.


* **Insérer une colonne** :

Insère une colonne avant ou après la colonne sur laquelle le curseur est
positionné.


* **Effacer une colonne** :

Supprime la colonne sur laquelle le curseur est positionné.


.. _editions_etat_lettretype_editeur-texte-riche-tableaux-insecables:

-------------------
Tableaux insécables
-------------------

Cette aide à la saisie n'est pas nécessairement disponible dans toutes les configurations de l'éditeur de texte riche (voir :ref:`editions_etat_lettretype_editeur-texte-riche-configurations`).

...


.. _editions_etat_lettretype_editeur-texte-riche-sauts-de-page:

-------------------------
Gestion des sauts de page
-------------------------

Le saut de page permet d'insérer un marqueur dans l'édition PDF, pour que le contenu qui suit soit positionné sur la page suivante.

Cette aide à la saisie n'est pas nécessairement disponible dans toutes les configurations de l'éditeur de texte riche (voir :ref:`editions_etat_lettretype_editeur-texte-riche-configurations`).

Pour ajouter un saut de page, il faut :

* positionner le curseur là où l'on souhaite l'insérer,
* cliquer sur l'élément 'Saut de page' du menu déroulant de l'éditeur de texte riche 'Insérer'.

Dans l'éditeur apparaît un rectangle avec des bordures en pointillés.



.. _editions_etat_lettretype_editeur-texte-riche-code-barres:

-----------------------
Gestion des code-barres
-----------------------

Cette aide à la saisie n'est pas nécessairement disponible dans toutes les configurations de l'éditeur de texte riche (voir :ref:`editions_etat_lettretype_editeur-texte-riche-configurations`).

Saisir le champ de fusion

Sélectionner le champ de fusion

Cliquer sur le bouton de génération du code-barres puis valider le formulaire
pour enregistrer les changements


.. _editions_etat_lettretype_editeur-texte-riche-majuscule-minuscule:

-------------------
Majuscule/Minuscule
-------------------

Cette aide à la saisie n'est pas nécessairement disponible dans toutes les configurations de l'éditeur de texte riche (voir :ref:`editions_etat_lettretype_editeur-texte-riche-configurations`).

...

.. _editions_etat_lettretype_editeur-texte-riche-plein-ecran:

---------------------------
Gestion du mode plein écran
---------------------------

Cette aide à la saisie n'est pas nécessairement disponible dans toutes les configurations de l'éditeur de texte riche (voir :ref:`editions_etat_lettretype_editeur-texte-riche-configurations`).

...

.. _editions_etat_lettretype_editeur-texte-riche-code-source:

-----------
Code source
-----------

Cette aide à la saisie n'est pas nécessairement disponible dans toutes les configurations de l'éditeur de texte riche (voir :ref:`editions_etat_lettretype_editeur-texte-riche-configurations`).

...

.. _editions_etat_lettretype_editeur-texte-riche-correction-orthographique:

-------------------------
Correction orthographique
-------------------------

Cette aide à la saisie n'est pas nécessairement disponible dans toutes les configurations de l'éditeur de texte riche (voir :ref:`editions_etat_lettretype_editeur-texte-riche-configurations`).

...

.. _editions_etat_lettretype_editeur-texte-riche-sous-etats:

-----------------------
Insertion de sous-états
-----------------------

Cette aide à la saisie n'est pas nécessairement disponible dans toutes les configurations de l'éditeur de texte riche (voir :ref:`editions_etat_lettretype_editeur-texte-riche-configurations`).

...


Sous-états
----------


Requêtes
--------


Logos
-----


.. _usage__administration__sig:

======
Le SIG
======

*option_localisation* : `sig_interne`

.. _usage__administration__sig__om_sig_extent:

Étendue
-------

Une étendue est une zone d'une carte (voir :ref:`usage__administration__sig__om_sig_map`) représentée par les coordonnées (x, y) de deux points (haut-gauche et bas-droit) d'un rectangle.

Le listing des étendues
=======================

.. image:: a_sig_etendue_listing.png

Le formulaire d'ajout d'un flux
===============================

.. image:: a_sig_etendue_form.png

- **om_sig_extent** ou **n°** : clé primaire numérique automatique.
- **nom** : nom de l'étendue permettant de la sélectionner / de la rechercher depuis le formulaire de paramétrage d'une carte.
- **extent** : coordonnées des points (haut-gauche et bas-droit) séparés par une virgule (exemple : ``1.38015228874262,49.8395709942243,2.10618821851606,50.3699899402347``).
- **valide** : inutile.


Les extent par défaut
=====================


Par défaut, il est proposé les extent  (data/pgsql/om_extent.sql) :

- des régions françaises

- des départements

- des arrondissements

- des EPCI

- des communes françaises

Par défaut, Arles et Vitrolles sont valides.


Récupération d'une étendue avec openStreetMap
=============================================

Faire une recherche sur OpenStreetMap http://www.openstreetmap.org/, ajuster la carte et cliquer sur le bouton ``Exporter``. Les coordonnées de l'étendue qui apparaissent doivent être récupérer dans l'ordre : gauche, haut, droite, bas.

.. image:: m_sig_osm_export.png


.. _usage__administration__sig__om_sig_flux:

Flux
----

Les flux permettent de créer des couches internes ou externes à l'application.
Il est traité les flux wms (web map service) et les flux de tuiles (tiles)

Le listing des flux
===================

.. image:: a_sig_flux_listing.png


Le formulaire d'ajout d'un flux
===============================

.. image:: a_sig_flux_form.png


- **om_sig_flux** : clé primaire numérique automatique
- **id** : identifiant unique du flux
- **libellé** : 
- **attribution** : permet d'afficher sur la carte l'attribution des données(copyright ou copyleft)
- **type de flux** ::

    - WMS
    - TCF - Flux TileCache (via openLayersLlayer.WMS) : tuiles
    - SMT - Slippy Map Tiles (type OSM)
    - IMP - Impression

- **url** : url du flux 
- **couches (séparées par des ,)** : saisir les couches séparées par des virgules

Paramètres (varie suivant le type de flux)

- si c'est un flux wms : vide
- si c'est une impression :  largeur carte dans composeur x 2 et hauteur carte dans composeur x 2
- si c'est des tuiles (TCF ou SMT) : URL pour GetFeatureInfo et couches pour GetFeatureInfo


Exemple d'url de flux wms (sur linux avec qgis) ::

    http://localhost/cgi-bin/qgis_mapserv.fcgi?SERVICE=WMS&VERSION=1.3.0&map=/var/www/openfoncier/trunk/app/qgis/openfoncier.qgs


Exemple d'url de flux de type impression (sur windows avec qgis) ::

    http://localhost/cartes/qgis/qgis_mapserv.fcgi.exe?SERVICE=WMS&VERSION=1.3.0&map=c:/osgeo4w/projetsqgis/openadresse/omp.qgs&REQUEST=GetPrint&TEMPLATE=A4PAYSAGE&map0:extent=[EXTENT]&SRS=EPSG%3A3857&WIDTH=210&HEIGHT=297&FORMAT=pdf&DPI=300&LAYERS=[LAYERS]



.. _usage__administration__sig__om_sig_map:

Carte
-----

Une carte est un affichage dans un contexte d'utilisation particulier.

Le listing des cartes
=====================

.. image:: a_sig_carte_listing.png

Le formulaire d'ajout d'une carte
=================================

.. image:: a_sig_carte_form.png


- **om_sig_map** : clé primaire numérique automatique

Titre :

- **id** : L'id est un identifiant unique permettant de d'identifier la carte dans les formulaires de saisie ou d'affichage.
- **libellé** : 
- **actif** : Pour modifier une carte, il est possible de créer la carte avec actif = non (éventuellement par copie) et ensuite la rendre active 'et désactiver l'ancienne). Cette méthode permet de retourner en arrière

Cas d'utilisation :

- **à partir d'un enregistrement donné** : cas des formulaires,
- **d'une requête mémorisée** : voir export,
- **d'une recherche** : dans le cadre d'une recherche (cas affichage).
- **flux en provenance de la carte** : Source flux permet de récupérer les flux wms d'une carte om_sig_map 'et d'éviter d'avoir à les resaisir.

Fond :

- **zoom** : le zoom d'affichage en fonction du centre
- **osm** : permet d'ajouter la couche OpenStreetMap (Simple OSM Map) aux couches de base de la carte,
- **bing** : permet d'ajouter les couches Bing (Bing Road, Bing Aerial et Bing Aerial + Labels) aux couches de base de la carte,
- **sat** : permet d'ajouter les couches Google (Google Hybrid, Google Satellite et Google RoadMap) aux couches de base de la carte,
- **layer_info** : Le "layer info" est constitué de marqueurs  issu de la requête SQL
- **fond par défaut** : Il est possible d'afficher un om_sig_map_flux par défaut en donnant son numéro. Il faut que le flux soit déclarer comme fond (???) dans om_sig_map_flux 

Paramètres cartographiques :

- **étendue** : L'etendue est la possibilité d'étendue par défaut de la carte (voir :ref:`usage__administration__sig__om_sig_extent`)
- **restreindre la navigation sur l'étendue** : Il est possible de restreindre la navigation à l'étendue
- **centre sur le point (si différent de l'étendue** : Il est possible de centrer sur un point différent que le centre de l'étendue en utilisant la carte om_sig_map. (vérifier)
- **projection** : Les choix de projection sont définis dans ``dyn/var_sig.inc``

Marqueurs : 

- **url** : l'url d'affichage de donnés
- **requête sql** : la requête dans le champ requête sql. Les marqueurs sont constitués à partir d'une requête et sont stockés dans un format geo json. Exemple de requête SQL pour affichage des marqueurs ::

    SELECT
        ST_asText(p.point_geom) AS geom,
        p.libelle AS titre,
        p.libelle AS description,
        p.pere AS idx,
        np.libelle AS nomenclature_lib,
        np.nomenclature_pere AS nomenclature_code
    FROM
        &DB_PREFIXEpere p 
            LEFT JOIN &DB_PREFIXEnomenclature_pere np
                ON np.nomenclature_pere = p.nomenclature_pere
    WHERE
        p.pere IN (SELECT &idx::integer UNION &lst_idx)

    -- variables
        &DB_PREFIXE = shema
        &idx = géométrie courante
        &lst_idx = liste des géométries courantes

    -- marqueur(s)
        Père 01aa aa (1)  titre + idx
        Père 01aa aa      description   
        nomenclature_lib: Nomenclature pere 02
        nomenclature_code: 02

SLD : 

- **fichier SLD pour les marqueurs** : données json
- **fichier SLD pour les données (om_sig_map_comp)** : données vecteurs

Autres :

- **retour** : inutilisé ?


.. _om_sig_map_comp:


Géométrie
=========

Une géométrie permet d'associer un ou plusieurs champs géométriques à la table correspondante du formulaire concerné. Ces champs géométriques peuvent être mis à jour dans l'interface. Ces champs constituent la couche vectorielle (modifiable) de la carte.

------------------------------
Le sous-listing des géométries
------------------------------

Le sous-listing est accessible depuis l'onglet *géométrie* dans le contexte d'une carte.

.. image:: a_sig_carte_onglet_geometrie_listing.png


-------------------------------------
Le formulaire d'ajout d'une géométrie
-------------------------------------

.. image:: a_sig_carte_onglet_geometrie_form.png


- **om_sig_map_comp** : clé primaire numérique automatique
- **om_sig_map** : clé primaire de la carte concernée (voir :ref:`usage__administration__sig__om_sig_map`)
- **nom géométrie** : nom de la géométrie (champ géométrique)
- **objet** :  objet concerné
- **ordre d'affichage** : 
- **actif** : 
- **mise à jour** : Si la mise à jour est autorisée (vrai), il faut saisir :
- **table** : la table du champ géométrique à modifier
- **chmp idx** : le nom de champ correspondant à la clé primaire de l'enregistrement à modifier
- **champ géographique** : le champ géographique à modifier
- **type de géométrie** : le type de géométrie


.. _om_sig_map_flux:

Flux appliqué à une carte
=========================

Un flux appliqué à une carte permet d'associer un ou plusieurs flux (voir :ref:`usage__administration__sig__om_sig_flux`) à une carte (voir :ref:`usage__administration__sig__om_sig_map`). L'idée principale est de permettre de configurer des couches/fonds disponibles sur une carte tout en factorisant la configuration des flux.

----------------------------------------------
Le sous-listing des flux appliqués à une carte
----------------------------------------------

Le sous-listing est accessible depuis l'onglet *flux appliqué à une carte* dans le contexte d'une carte.

.. image:: a_sig_carte_onglet_flux_listing.png


----------------------------------------------------
Le formulaire d'ajout d'un flux appliqué à une carte
----------------------------------------------------

.. image:: a_sig_carte_onglet_flux_form.png


- **om_sig_map_flux** : clé primaire numérique automatique
- **om_sig_map** : clé primaire de la carte concernée (voir :ref:`usage__administration__sig__om_sig_map`)
- **om_sig_flux** : flux wms permet d'associer un flux wms (voir :ref:`usage__administration__sig__om_sig_flux`) à la carte (voir :ref:`usage__administration__sig__om_sig_map`)
- **nom map openLayer** : le nom map openLayers sera le nom qui apparaitra sur la carte dans l'onglet "couche" ou "fond"

Paramètres généraux :

- **fond de carte** : fond de carte permet d'associer le flux comme un fond
- **niveau de zoom maximum** : 
- **ordre** : permet de gérer l'ordre d'apparition des couches (ou fond) dans l'onglet correspondand
- **visible par défaut** : affiche la couche si c'est vrai à l'ouverture de la carte
- **singletile OL** : raméne le flux wms en une seule image pour la fenêtre et non en une série d'imagette, ce qui permet de résoudre le problème des étiquettes tronquées. ATTENTION les temps de réponses peuvent s'allonger car le cache ( du serveur wms ou du navigateur ???) n'est pas utilisé


Paramètres avancés :

- **panier** : le panier permet de stocker des géométries en vue de définir la géométrie de l'objet en cours : exemple : un permis de construire est un ensemble de parcelles
- **nom du panier** : exemple  : panier parcelle
- **couche du panier** : désigner la couche (layer) à utiliser : parcelle
- **attribut de la couche du panier** : désigner l'attribut à récupérer : ex: parcelle
- **caractère d'encapsulationde valeur panier** : définir le caractère d'encapsulation (en général la ' si alpha numérique ??? et vide si numérique)
- **requête d'union (&lst) du panier** : construire la requête d'union qui va être la nouvelle géométrie
- **type de géométrie du panier** : donner le type de géométrie : le plus souvant un polygone
- **requête de filtrage ($idx)** : définir la requête de filtrage qui va permettre de filter le flux wms. Exemple d'une requête construisant une géométrie faisant une union des parcelles séléctionnées dans le panier ::    

    select st_astext(st_union(geom)) as geom from &DB_PREFIXEparcelle
        where parcelle in (&lst)

    &DB_PREFIXE = schéma
    &lst = liste des géométrie du panier

    Exemple d'une requête filtrée :

    pour produire le filtre suivant :
    layer1:"champ1" = 'valeur1',layer2:"champ2" = 'valeur2'

    il faut entrer la requête suivante pour selectionner les electeurs d'un bureau :
    
    select 'electeur:²bureau² = '''||bureau.bureau||''' as buffer
        from &DB_PREFIXEbureau where bureau = '&idx'
    
    
    select 'electeur:²bureau² = ''&idx'' as buffer from &DB_PREFIXEbureau
        where bureau = '&idx'
    
    -- parametres
    ² = caractère utilisé pour les doubles quotes : "
    || concatenation sql
    ''' permet d echapper la simple quote
    '' sql remplace les deux quotes par une quote (caractere quote)
    
    le filtre final appliqué au flux wms est : electeur:"bureau" = '04'  pour le bureau 04


    autre exemple le père et tous ses fils

    SELECT 'fpere_point:²pere² IN ( '||pere||' );fpere_perim:²pere² IN ( '||pere||' );ffils_point:²pere²
    IN ( '||pere||' );ffils_point:²pere² IN ( '||pere||' );ffils_perim:²pere² IN ( '||pere||' )'
    AS buffer FROM ( SELECT array_to_string(array_agg(pere), ' , ') AS pere FROM &DB_PREFIXEpere
    WHERE pere IN (SELECT &idx::integer UNION &lst_idx) ) a

