.. _reqmo:

##############
Module 'Reqmo'
##############

.. contents::

========
Principe
========

Le développeur/intégrateur met à disposition de l'utilisateur un ensemble de
requêtes mémorisées. C'est-à-dire des écrans permettant d'exporter des données
définies grâce un fichier de variables. Pour chaque requête mémoriée l'utilisateur
peut sélectionner :

- la liste des colonnes à exporter ou non,
- le ou les filtres de données,
- le tri,
- le format de sortie :

  - Tableau - Affichage à l'écran (choix du nombre d'enregisrements à afficher),
  - CSV - Export vers le logiciel tableur (séparateurs : ';' (point-virgule), '|' (pipe), ',' (virgule).


L'intégralité du module est accessible via le menu 'Export' -> 'Requêtes Mémorisées'.

- Listing de toutes les requêtes mémorisées disponibles :

  .. image:: a_reqmo_listing_des_exports_disponibles.png

- Formulaire d'export d'une requête mémorisée :

  .. image:: a_reqmo_formulaire_exemple.png

- L'affichage de l'export :

  - Format de sortie 'tableau'

    .. image:: a_reqmo_affichage_sortie_tableau.png

  - Format de sortie 'csv'

    .. image:: a_reqmo_affichage_sortie_csv.png


=============
Configuration
=============

Une reqmo se configure via le script ``sql/pgsql/<OBJ>.reqmo.inc.php``.


Les paramètres sont :

- $reqmo['libelle'] contient le libéllé affiché en haut
- $reqmo['sql'] contient la requête SQL.


Dans la requête, les paramétres sont mis entre [] et ils sont définis en dessous sous la forme $reqmo[parametre]=.

- "checked" : la colonne est affiché ou non
- "un tableau" array(a,b) et le choix a ou b est donné à l utilisateur de requête
- "une requête sql" : le choix se fait dans la table du select

La requête executée est celle qui est reconstituée avec les zones sasisies par l'utilisateur.

Exemple d'une reqmo pour les voies dans l'application openCimetière :

.. code-block:: php

    <?php
    $reqmo = array();
    $reqmo["libelle"] = " Voies par cimetiere ";
    $reqmo["sql"] = "
        SELECT voie, voietype, voielib, [zonetype], [zonelib], [cimetierelib]
        FROM voie
            INNER JOIN zone ON voie.zone=zone.zone
            INNER JOIN cimetiere ON zone.cimetiere=cimetiere.cimetiere
        WHERE cimetiere.cimetiere=[cimetiere]
        ORDER BY [tri]
    ";
    //
    $reqmo["zonetype"] = "checked";
    $reqmo["zonelib"] = "checked";
    $reqmo["cimetierelib"] = "checked";
    //
    $reqmo["tri"] = array("voielib", "zonelib", );
    //
    $reqmo["cimetiere"] = "select cimetiere, concat(cimetiere, ' ', cimetierelib) FROM cimetiere";
    ?>


==========
Composants
==========

Les composants du framework qui gèrent le module 'reqmo' sont :

* ``OM_ROUTE_MODULE_REQMO``
* ``application::view_module_reqmo()``
* ``application::get_inst__om_reqmo()``
* :file:`core/om_reqmo.class.php`

