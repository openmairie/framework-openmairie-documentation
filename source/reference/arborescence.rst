.. _arborescence:

############
Arborescence
############

.. contents::

============
Introduction
============

Cette rubrique vise à décrire brièvement l'arborescence du framework pour
comprendre l'objectif de chaque répertoire. Elle est divisée en deux parties :
les répertoires spécifiques à l'applicatif qui sont modifiés lors du
développement de l'applicatif et les répertoires du framework qui sont récupérés
tel quel dans le framework.

``[F] .htaccess`` dans les descriptions de répertoires ci-dessous représente des
fichiers .htaccess empêchant l'accès dans les répertoires en question qui ne
doivent pas être accessibles depuis l'interface par l'utilisateur.


==========================================
Les répertoires spécifiques à l'applicatif
==========================================

Ces répertoires sont ceux qui font l'applicatif. Par exemple, sur un applicatif
comme openCimetière ce sont les répertoires qui vont se trouver dans le gestionnaire de versions
(http://adullact.net/scm/viewvc.php/opencimetiere/trunk/) 
que l'on appelle les répertoires spécifiques.


:file:`app/`
------------

Contient tout les scripts spécifiques à l'applicatif que ce soit des
javascripts ou des images ou des scripts PHP ::

    [D] app/ 
     |-[D] css/
        |---- ...
     |-[D] img/
        |---- ...
     |-[D] js/
        |---- ...
     |-[F] index.php
     |---- ...


:file:`data/`
-------------

Contient tous les fichiers d'initialisation de la base de données de
l'applicatif ::

    [D] data/
     |-[F] .htaccess
     |-[D] pgsql/
        |---- ...
     |---- ...



:file:`dyn/`
------------

Contient les fichiers de paramétrage de l'applicatif ::

    [D] dyn
     |-[F] .htaccess
     |---- ...

:file:`gen/`
------------

Contient les scripts (obj) et requêtes (sql) générés par le générateur ::

    [D] gen
     |-[F] .htaccess
     |-[D] dyn/
        |---- ...
     |-[D] inc/
        |---- ...
     |-[D] obj/
        |---- ...
     |-[D] sql/
        |-[D] pgsql
        |---- ...
     |---- ...


:file:`locales/`
----------------

Contient les fichiers de traduction de l'applicatif ::

    [D] locales/
     |-[F] .htaccess
     |---- ...


:file:`obj/`
------------

Contient les objets métiers surchargeant les objets générés ::

    [D] obj/
     |-[F] .htaccess
     |---- ...


:file:`sql/`
------------

Contient les scripts sql surchargeant les scripts générés ::

    [D] sql/
     |-[F] .htaccess
     |-[D] pgsql/
        |---- ...
     |---- ...


:file:`tests/`
--------------

Contient les jeux de tests unitaires et fonctionnels de l'applicatif ::

    [D] tests/
     |-[F] .htaccess
     |---- ...

.. _arborescence_repertoire_var:

:file:`var/`
------------

Contient les fichiers de logs, les fichiers temporaires et les fichiers stockés par l'applicatif ::

    [D] var/
     |-[F] .htaccess
     |-[D] log/
        |-[F] error.log
        |---- ...
     |-[D] filestorage/
        |---- ...
     |-[D] tmp/
        |---- ...
     |---- ...


============================
Les répertoires du framework
============================

Ces répertoires sont issus du framework, c'est-à-dire qu'ils ne sont pas dans l'applicatif
lui même. Par exemple, sur un applicatif comme openCimetière ce sont les répertoires qui
vont être récupérés par une propriété externals sur le gestionnaire de versions 
(http://adullact.net/scm/viewvc.php/opencimetiere/trunk/EXTERNALS.txt?view=markup)
que l'on appelle les répertoires du framework.


:file:`core/`
-------------

Contient les classes de la librairie du framework ::

    [D] core
     |-[F] .htaccess
     |---- ...


:file:`php/`
------------

Contient les librairies PHP utilisées par le framework (comme dbpear, phpmailer
ou fpdf) ::

    [D] php
     |-[F] .htaccess
     |---- ...


:file:`lib/`
------------

Contient les librairies javascripts utilisées par le framework (comme openLayers
ou jquery) ::

    [D] lib/
     |-[D] om-assets/
        |-[D] css/
        |-[D] img/
        |-[D] js/
        |---- ...
     |---- ...


====================================
Les anciens répertoires du framework
====================================


:file:`css/`
------------

.. warning::

    Ce répertoire a été déplacé dans ``lib/om-assets/`` depuis la version 4.7.0.

Contient les feuilles de style de base du framework ::

    [D] css/
     |---- ...


:file:`img/`
------------

.. warning::

    Ce répertoire a été déplacé dans ``lib/om-assets/`` depuis la version 4.7.0.

Contient les images du framework ::

    [D] img/
     |---- ...


:file:`js/`
-----------

.. warning::

    Ce répertoire a été déplacé dans ``lib/om-assets/`` depuis la version 4.7.0.

Contient les javascripts de base du framework ::

    [D] js/
     |---- ...


:file:`pdf/`
------------

.. warning::

    Ce répertoire a été supprimé depuis la version 4.6.0.

Contient les scripts d'édition du framework ::

    [D] pdf/
     |---- ...


:file:`scr/`
------------

.. warning::

    Ce répertoire a été supprimé depuis la version 4.7.0.

Contient les scripts d'affichage du framework ::

    [D] scr/
     |---- ...


:file:`spg/`
------------

.. warning::

    Ce répertoire a été supprimé depuis la version 4.7.0.

Contient les sous programmes génériques du framework ::

    [D] spg/
     |---- ...

